FROM quay.io/fenicsproject/stable:latest

MAINTAINER vmasha

# install debian packages
RUN apt-get update -qq \
 && apt-get install --no-install-recommends -y \
    # install essentials
    build-essential \
    g++ \
    git \
    openssh-client \
    # install python 2
    python \
    python-dev \
    python-pip \
    python-setuptools \
    python-virtualenv \
    python-wheel \
    pkg-config \
    # requirements for numpy
    libopenblas-base \
    python-numpy \
    python-scipy \
    # requirements for keras
    python-h5py \
    python-yaml \
    python-pydot \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

# manually update numpy
RUN pip --no-cache-dir install -U numpy

# manually install tensorflow
RUN pip --no-cache-dir install tensorflow

RUN pip --no-cache-dir install keras

# manually install opencv
RUN pip --no-cache-dir install opencv-python

WORKDIR /srv/
